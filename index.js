// alert("Hello 189!")





let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

let studentID = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];
console.log(studentID);

/*
	Arrays
		- used to store multiple related values in a single variable
		- declared using the square brackets ([]) also know as "Array Literals"

		Syntax: 
			let/const arrayName = [elementA, elementB, ... elementN]

*/

let grades = [95.6, 98.3, 87.6, 90.4]
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]

let mixedArr = [12, "Asus", null, undefined, {}]; // not recommended
console.log(mixedArr)


let myTasks = [
		"drink html",
		"eat javascript",
		"inhale css",
		"bake sass"
];

console.log(myTasks)

// We can also store values of separate varaibles in an array
let city1 = "Tokyo"
let city2 = "Manila"
let city3 = "jakarta"

let cities = [city1, city2, city3];
// let citySample = ["Tokyo", "Manila", "Jakarta"];
console.log(cities);
// console.log(cities == citySample)


// Array Length Propery
console.log(myTasks.length) //4
console.log(cities.length) //3

let blankArr = [];
console.log(blankArr.length) // result is 0 because  the array is empty

let fullName = "Jaime Noble"
console.log(fullName.length) //11

myTasks.length = myTasks.length - 1
console.log(myTasks.length)
console.log(myTasks)

cities.length--
console.log(cities.length)
console.log(cities)

// Can we delete a character in a string using .length property?
console.log(fullName.length)
fullName.length = fullName.length - 1
console.log(fullName.length)

fullName.length--
console.log(fullName)
// Answer = no, we can only delete or add using .length property in arrays


let theBeatles = ["John", "Paul", "Ringo", "Geroge"]
console.log(theBeatles.length)
theBeatles.length++
console.log(theBeatles.length)
console.log(theBeatles)

// arrayName[i] = "new value"
theBeatles[4] = "Rupert"
console.log(theBeatles)


/*
	Accessing Elements of an Array
	
	Syntax:
		arrayName[index]

*/

console.log(grades[0])
console.log(computerBrands[3])


console.log(grades[20])
grades[20] = 90;
console.log(grades[20]) // can add value to specific index


let lakerLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"]
console.log(lakerLegends[1])
console.log(lakerLegends[3])


// Can we save array items in another variable?

let currentLaker = lakerLegends[2]
console.log(currentLaker)


console.log("Arrays before reassignment");
console.log(lakerLegends);
lakerLegends[2] = "Pau Gasol";
console.log("Arrays after reassignment")
console.log(lakerLegends);

// Accessing the last element of an array

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]

let lastElementIndex = bullsLegend.length - 1
console.log(bullsLegend[lastElementIndex])

console.log(bullsLegend[bullsLegend.length - 1])
console.log(grades)


// Adding Items into the Array

let newArr = []
console.log(newArr)
console.log(newArr[0])
newArr[0] = "Jenny"
console.log(newArr)
newArr[1] = "Jisoo";
console.log(newArr)



//What if we want to add an element at the end of our array?

newArr[newArr.length++] = "Lisa"
console.log(newArr)


// Loops over an Array

for(let index = 0; index < newArr.length; index++) {
	console.log(newArr[index])
}

let numArr = [5, 12, 30, 46, 50, 88]

for(let index = 0; index < numArr.length; index++) {

	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5.")
	} else {
		console.log(numArr[index] + " is not divisible by 5.")
	}
}


// Multidimensional Array

let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]

]

console.log(chessBoard)
console.log(chessBoard[1][4])
console.log("Pawn moves to: " + chessBoard[7][4])


